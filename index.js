const path = require("path");
const plugin = require("@parcel/plugin");
const slm = require("slm");

module.exports = new plugin.Transformer({
	async loadConfig({config}) {
		let cfg = await config.getConfig(
			["slm.config.js"],
			{packageKey: "slm"});

		return cfg?.contents || {};
	},

	async transform({asset, config}) {
		const vm = new slm.template.VM();

		const content = await asset.getCode();
		const render = slm.compile(content, {
			basePath: path.dirname(asset.filePath),
			filename: asset.filePath,
			...config,
		}, vm);

		asset.type = "html";
		asset.setCode(render(config.locals));

		for (let filePath in vm._cache) {
			await asset.addIncludedFile(filePath);
		}

		return [asset];
	},
});
