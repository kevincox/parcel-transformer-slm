# parcel-transformer-slm

A [Parcel 2](https://parceljs.org/) [transformer](https://github.com/parcel-bundler/parcel/tree/v2/#transforms-1) for [slm](https://github.com/slm-lang/slm).

## Notable Missing Features

- Imports are not resolved via parcel. This means that all inputs such as partials must be real files on disk.
- Currently relies on a private interface to identify imported files.
- Ability to compile to client-side templates.

## Configuration

Enable the transformer:

```js
// .parcelrc
{
	extends: ["@parcel/config-default"],
	transforms: {
		"*.slm": ["parcel-transformer-slm"],
	},
}
```

Configure slm:

```js
// slm.config.js
module.exports = {
	format: "html",
};
```

## Stability

This package follows semver.

- Changes to the exact HTML output is not considered a breaking change.
